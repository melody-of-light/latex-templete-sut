## 沈阳工业大学硕士学位论文LaTeX模板

本模板是基于[北京理工大学LaTeX模板](https://github.com/BIT-thesis/LaTeX-template)，按照沈阳工业大学硕士学位论文的格式要求（20220516）修改而来，目前只包含摘要到最后致谢部分，打印时要双面打印。限于作者水平，此模板难免有不足之处，不能保证完全符合要求，仅供参考。

### 使用说明
#### 安装环境

1.下载[Tex Live](http://tug.org/texlive/)

2.下载[VS Code](https://code.visualstudio.com/)

3.参照[安装教程](https://zhuanlan.zhihu.com/p/36285613)进行安装

#### 使用方法

可以参考上面北理工模板的教程
