% 表格换行功能
\usepackage{makecell}

%% math packages -- conflict with xunicode
\RequirePackage{amsmath,amsthm,amsfonts,amssymb,bm,mathrsfs}
% 直立希腊字母字体
\RequirePackage{upgreek}

\NeedsTeXFormat{LaTeX2e}[2005/12/01]
\ProvidesClass{SUT-thesis-grd}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{ctexbook}}
\ProcessOptions\relax
\LoadClass[zihao=-4,a4paper,twoside,openany,UTF8,space=auto]{ctexbook}

%%
%% the setup of ctex package
%%
\def\contentsname{目录}
\def\listfigurename{插图}
\def\listtablename{表格}

%% 设置圆圈的格式 或使用\textcircled
\usepackage{tikz}
\usepackage{etoolbox}
\newcommand{\circled}[2][]{\tikz[baseline=(char.base)]
    {\node[shape = circle, draw, inner sep = 1pt]
    (char) {\phantom{\ifblank{#1}{#2}{#1}}};
    \node at (char.center) {\makebox[0pt][c]{#2}};}}
\robustify{\circled}

%% 变量
\def\SUT@label@abstract{摘要}
\def\SUT@label@englishabstract{Abstract}
\def\SUT@label@keywords{关键词：}
\def\SUT@label@englishkeywords{Key Words:~}
\def\SUT@label@conclusion{结论}
\def\SUT@label@appendix{附录}
\def\SUT@label@publications{在学研究成果}
\def\SUT@label@projects{攻读学位期间参与的项目}
\def\SUT@label@resume{作者简介}
\def\SUT@label@reference{参考文献！！！！}
\def\SUT@label@thanks{致谢}
%%
%% label in the head 页眉页脚
%%
\def\SUT@label@headschoolname{沈阳工业大学硕士学位论文}

%% 当前模板的版本
\newcommand{\version}{\SUT@value@templateversion}

%%==============引用geometry 宏包设置纸张和页面========================
\usepackage[%
paper=a4paper,%
top=2.5cm,% 上 2.5cm %
bottom=2.2cm,% 下 2.2cm %
left=3cm,% 左 2.5+0.5cm %
right=2.2cm,% 右 2.2cm %
headsep=0.35cm,
headheight=0.4cm,% 页眉 1.75cm %
footskip=0.7cm% 页脚 1.75cm %
]{geometry} % 页面设置 %
%% =========================================================
\ziju{0.03}

% \parskip 0.5ex plus 0.25ex minus 0.25ex
%% Command -- Clear Double Page
\def\cleardoublepage{\clearpage\if@twoside \ifodd\c@page\else
  \thispagestyle{empty}%
  \hbox{}\newpage\if@twocolumn\hbox{}\newpage\fi\fi\fi}
%设置行距，大概为22榜
\RequirePackage{setspace}
\setstretch{1.523}

% 设置宋体加粗样式
\setCJKfamilyfont{zhsongb}[AutoFakeBold = {2.17}]{SimSun}
\newcommand{\songtib}{\CJKfamily{zhsongb}}
% 设置字体族
\setCJKsansfont{SimHei}

\setmainfont{Times New Roman}
\setsansfont{Arial}
\setmonofont{Courier New}
% 创建字体族
\newfontfamily\entimes[AutoFakeBold = {2.17}]{Times New Roman}
\newfontfamily\enheiti{SimHei}
\newfontfamily\ensongti[AutoFakeBold = {2.17}]{SimSun}

\newcommand{\chaptermd}[1]{\chapter{\mdseries#1}}
%% 设置章节格式, 黑体三号居中，行距22磅，与正文或节标题的间距设定为段后间距1行。章序号与章名间空一格。
\ctexset{chapter={
      name = {第,章},
      number = {\arabic{chapter}},
      numberformat = {\mdseries},
      %format = {\bfseries \enheiti \heiti \centering \zihao{3}},
      format = {\bfseries \heiti \centering \zihao{3}},
      pagestyle = {plain},
      beforeskip = 16 bp,
      afterskip = 32 bp,
      fixskip = true,
      aftername = \enspace
  }
}
%% 设置一级章节格式
% 黑体四号顶左，1.5倍行距，与上一节的间距为0.5行，与下面正文或节标题的段间间距为0行。序号与题目间空一格。
\ctexset{section={
  %format={\raggedright \enheiti \heiti \zihao{4}},
  format={\raggedright \heiti \zihao{4}},
  beforeskip = 12bp,
  afterskip = 17bp,
  fixskip = true,
  aftername = \enspace
  }
}

% 设置二级标题格式
% 宋体小四加粗顶左，1.5倍行距，与上一节的间距为0.5行，与下面正文或节标题的段间间距为0行。序号与题目间空一格。 
\ctexset{subsection={
   %format = {\songtib \ensongti \bfseries \raggedright \zihao{-4}},
   format = {\songtib \bfseries \raggedright \zihao{-4}},
   beforeskip = 12bp,
   afterskip = 17bp,
   fixskip = true,
   aftername = \enspace
   }
}

% 设置三节标题格式
\ctexset{subsubsection={
      format={\songti \raggedright \zihao{-4}},
      beforeskip=28bp plus 1ex minus .2ex,
      afterskip=24bp plus .2ex,
      fixskip=true,
  }
}

%% 设定目录格式。目录颜色更改黑色
\addtocontents{toc}{\protect\hypersetup{hidelinks}}
\addtocontents{lot}{\protect\hypersetup{hidelinks}}
\addtocontents{lof}{\protect\hypersetup{hidelinks}}

\RequirePackage{titletoc}

% \titlecontents{chapter}[0pt]{\enheiti \heiti \zihao{-4}}
%     {\thecontentslabel \ }
%     {\mdseries}
%     {\hspace{.5em}\rmfamily\titlerule*[.25em]{.}\contentspage}[\vspace{1pt}]

% \titlecontents{section}[\ccwd]{\ensongti \songti \zihao{-4}}
%     {\enheiti \thecontentslabel \ensongti \hspace{.5em}}{}
%     {\hspace{.5em}\rmfamily\titlerule*[.25em]{.}\contentspage}[\vspace{1pt}]

% \titlecontents{subsection}[2\ccwd]{\ensongti \songti \zihao{-4}}
%     {\thecontentslabel \hspace{.5em}}{}
%     {\hspace{.5em}\rmfamily\titlerule*[.25em]{.}\contentspage}[\vspace{1pt}]

\titlecontents{chapter}[0pt]{\heiti \zihao{-4}}
    {\thecontentslabel \ }
    {\mdseries}
    {\hspace{.5em}\rmfamily\titlerule*[.25em]{.}\contentspage}[\vspace{1pt}]

\titlecontents{section}[21pt]{\zihao{-4}}
    {\thecontentslabel \hspace{.5em}}{}
    {\hspace{.5em}\rmfamily\titlerule*[.25em]{.}\contentspage}[\vspace{1pt}]

\titlecontents{subsection}[42pt]{\zihao{-4}}
    {\thecontentslabel \hspace{.5em}}{}
    {\hspace{.5em}\rmfamily\titlerule*[.25em]{.}\contentspage}[\vspace{1pt}]

\titlecontents{figure}[0pt]{\songti\zihao{-4}}
    {\figurename~\thecontentslabel\ }{\hspace*{-1.5cm}}
    {\hspace{.5em}\titlerule*{.}\contentspage}

\titlecontents{table}[0pt]{\songti\zihao{-4}}
    {\tablename~\thecontentslabel\ }{\hspace*{-1.5cm}}
    {\hspace{.5em}\titlerule*{.}\contentspage}

%% 选择编译
\RequirePackage{ifthen}

%% check pdfTeX mode
\RequirePackage{ifpdf}

%% fancyhdr 页眉页脚控制
\RequirePackage{fancyhdr}

% 空 页眉页脚
\fancypagestyle{SUT@empty}{%
  \fancyhf{}}

%======正文页眉页脚=================
\fancypagestyle{SUT@headings}{%
  \fancyhf{}
  \renewcommand{\headrulewidth}{0.5pt}
  \fancyfoot[C]{\songti\zihao{5} \thepage}
  \if@twoside
    \fancyhead[CO]{\songti \zihao{5} {\leftmark}}
    \fancyhead[CE]{\zihao{5}{\songti 沈阳工业大学硕士学位论文}}
  \else
    \fancyhead[C]{\zihao{5}{\songti 沈阳工业大学硕士学位论文}}
  \fi
}

%==================================对于openright 选项，必须保证章页右开，且如果前章末页内容须清空其页眉页脚。===================
\let\SUT@cleardoublepage\cleardoublepage
\newcommand{\SUT@clearemptydoublepage}{%
	\clearpage{\pagestyle{SUT@empty}\SUT@cleardoublepage}}
\let\cleardoublepage\SUT@clearemptydoublepage

 %================修该frontmatter 的页码为大写罗马格式，并调整页面风格===============
\renewcommand{\frontmatter}{
 \if@openright\cleardoublepage\else\clearpage\fi
  \@mainmatterfalse
  \pagenumbering{Roman}
  %\pagestyle{plain}
  \pagestyle{SUT@headings}
}
%=======================修改mainmatter 的页码为阿拉伯格式，并调整页面风格========================
\renewcommand{\mainmatter}{
  %\if@openright\cleardoublepage\else\clearpage\fi
  \cleardoublepage
  \@mainmattertrue
  \pagenumbering{arabic}
  \pagestyle{SUT@headings}
}

%% 复杂表格
\RequirePackage{threeparttable}
\RequirePackage{dcolumn}
\RequirePackage{multirow}
\RequirePackage{booktabs}
\newcolumntype{d}[1]{D{.}{.}{#1}}% or D{.}{,}{#1} or D{.}{\cdot}{#1}

%% 定义几个常用的数学常量符号
\newcommand{\me}{\mathrm{e}} %定义 对数常数e，虚数符号i,j以及微分算子d为直立体。
\newcommand{\mi}{\mathrm{i}}
\newcommand{\mj}{\mathrm{j}}
\newcommand{\dif}{\,\mathrm{d}} 

\theoremstyle{plain}
  \newtheorem{algo}{算法~}[chapter]
  \newtheorem{thm}{定理~}[chapter]
  \newtheorem{lem}[thm]{引理~}
  \newtheorem{prop}[thm]{命题~}
  \newtheorem{cor}[thm]{推论~}
\theoremstyle{definition}
  \newtheorem{defn}{定义~}[chapter]
  \newtheorem{conj}{猜想~}[chapter]
  \newtheorem{exmp}{例~}[chapter]
  \newtheorem{rem}{注~}
  \newtheorem{case}{情形~}
\renewcommand{\proofname}{\bf 证明}

%% 英文字体使用 Times New Roman
\RequirePackage{xltxtra} % \XeTeX Logo

%% graphics packages
\RequirePackage{graphicx}
%% 并列子图
\RequirePackage{subfigure}

\RequirePackage{wrapfig}
%%===========================设置图表标题选项==========================
\RequirePackage{amsmath}
\RequirePackage{caption}
\DeclareCaptionLabelSeparator{zhspace}{\hspace{1\ccwd}}
\DeclareCaptionFont{fontsize}{\zihao{5}}
\captionsetup{
  justification=centering,
  font = {fontsize},
  labelsep = zhspace,
}
\captionsetup[table]{
  position = top,
  aboveskip = 6bp,
  belowskip = 6bp,
}
\numberwithin{table}{chapter}
\captionsetup[figure]{
  position = bottom,
  aboveskip = 6bp,
  belowskip = 6bp,
}

%% 如果插入的图片没有指定扩展名，那么依次搜索下面的扩展名所对应的文件
\DeclareGraphicsExtensions{.pdf,.eps,.png,.jpg,.jpeg}
% ccaption -- bicaption
%\RequirePackage{ccaption}
%\captiondelim{\ }
%\captionnamefont{\songti\zihao{5}}
%\captiontitlefont{\songti\zihao{5}}

%% sort and compress citations
\RequirePackage[numbers,square,comma,super,sort&compress]{natbib}
% 上标引用
%\newcommand{\upcite}[1]{\textsuperscript{\cite{#1}}}
\newcommand{\upcite}[1]{\cite{#1}}
\newcommand{\downcite}[1]{\scalebox{1.3}[1.3]{\raisebox{-0.65ex}{\cite{#1}}}}

% 将浮动参数设为较宽松的值
\renewcommand{\textfraction}{0.15}
\renewcommand{\topfraction}{0.85}
\renewcommand{\bottomfraction}{0.65}
\renewcommand{\floatpagefraction}{0.60}


% 定公式、图、表编号为"3-1"的形式，即分隔符由.变为短杠
\renewcommand\theequation{\arabic{chapter}.\arabic{equation}}
\renewcommand\thefigure{\arabic{chapter}.\arabic{figure}}
\renewcommand\thetable{\arabic{chapter}.\arabic{table}}

% 颜色宏包
\RequirePackage{xcolor}


% 中文破折号
\newcommand{\cndash}{\rule{0.0em}{0pt}\rule[0.35em]{1.4em}{0.05em}\rule{0.2em}{0pt}}

% listings 源代码显示宏包
\RequirePackage{listings}
\lstset{tabsize=4, %
  frame=shadowbox, %把代码用带有阴影的框圈起来
  commentstyle=\color{red!50!green!50!blue!50},%浅灰色的注释
  rulesepcolor=\color{red!20!green!20!blue!20},%代码块边框为淡青色
  keywordstyle=\color{blue!90}\bfseries, %代码关键字的颜色为蓝色，粗体
  showstringspaces=false,%不显示代码字符串中间的空格标记
  stringstyle=\ttfamily, % 代码字符串的特殊格式
  keepspaces=true, %
  breakindent=22pt, %
  numbers=left,%左侧显示行号
  stepnumber=1,%
  numberstyle=\tiny, %行号字体用小号
  basicstyle=\footnotesize, %
  showspaces=false, %
  flexiblecolumns=true, %
  breaklines=true, %对过长的代码自动换行
  breakautoindent=true,%
  breakindent=4em, %
  aboveskip=1em, %代码块边框
  %% added by http://bbs.ctex.org/viewthread.php?tid=53451
  fontadjust,
  captionpos=t,
  framextopmargin=2pt,framexbottommargin=2pt,abovecaptionskip=-3pt,belowcaptionskip=3pt,
  xleftmargin=4em,xrightmargin=4em, % 设定listing左右的空白
  texcl=true,
  % 设定中文冲突，断行，列模式，数学环境输入，listing数字的样式
  extendedchars=false,columns=flexible,mathescape=true
  numbersep=-1em
}
\renewcommand{\lstlistingname}{代码} %% 重命名Listings标题头


%% hyperref package
\definecolor{navyblue}{RGB}{0,0,128} 
\RequirePackage{hyperref}
\hypersetup{
  bookmarksnumbered,%
  linktoc=all,
  colorlinks=true,
  citecolor=black,
  filecolor=black,
  linkcolor=black,
  linkbordercolor=black,
  urlcolor=black,
  plainpages=false,%
  pdfstartview=FitH
}

%% enumerate 列表环境间距调节
\usepackage{enumitem}
% \setenumerate[1]{itemsep=0pt,partopsep=0pt,parsep=\parskip,topsep=5pt}
% \setitemize[1]{itemsep=0pt,partopsep=0pt,parsep=\parskip,topsep=0pt}
% \setdescription{itemsep=0pt,partopsep=0pt,parsep=\parskip,topsep=5pt}

%_  参考文献风格
%\bibliographystyle{thuthesis-numeric}
%\bibliographystyle{GBT7714-2005NLang}
%\bibliographystyle{GBT7714-2005}
\bibliographystyle{gbt7714-2005-sut}

%_ SUTspace
%\newcommand\SUTspace{\protect\CTEX@spaceChar\protect\CTEX@spaceChar}
\newcommand{\SUTspace}[1][1]{\hspace{#1\ccwd}}

\def\SUT@getfileinfo#1 #2 #3\relax#4\relax{%
  \def\SUTfiledate{#1}%
  \def\SUTfileversion{#2}%
  \def\SUTfileinfo{#3}}%
\expandafter\ifx\csname ver@SUTmaster-xetex.cls\endcsname\relax
  \edef\reserved@a{\csname ver@ctextemp_SUTmaster-xetex.cls\endcsname}
\else
  \edef\reserved@a{\csname ver@SUTmaster-xetex.cls\endcsname}
\fi
\expandafter\SUT@getfileinfo\reserved@a\relax? ? \relax\relax
\def\SUT@underline[#1]#2{%
  \underline{\hbox to #1{\hfill#2\hfill}}}
\def\SUTunderline{\@ifnextchar[\SUT@underline\underline}

%%%%中文标题页的可用命令
\newcommand\classification[1]{\def\SUT@value@classification{#1}}
\newcommand\studentnumber[1]{\def\SUT@value@studentnumber{#1}}
\newcommand\confidential[1]{\def\SUT@value@confidential{#1}}
\newcommand\UDC[1]{\def\SUT@value@UDC{#1}}
\newcommand\serialnumber[1]{\def\SUT@value@serialnumber{#1}}
\newcommand\school[1]{\def\SUT@value@school{#1}}
\newcommand\degree[1]{\def\SUT@value@degree{#1}}
\renewcommand\title[2][\SUT@value@title]{%
  \def\SUT@value@title{#2}
  \def\SUT@value@titlemark{\MakeUppercase{#1}}}

\newcommand\vtitle[1]{\def\SUT@value@vtitle{#1}}
\renewcommand\author[1]{\def\SUT@value@author{#1}}
\newcommand\advisor[1]{\def\SUT@value@advisor{#1}}
\newcommand\advisorinstitute[1]{\def\SUT@value@advisorinstitute{#1}}
\newcommand\major[1]{\def\SUT@value@major{#1}}
\newcommand\submitdate[1]{\def\SUT@value@submitdate{#1}}
\newcommand\defenddate[1]{\def\SUT@value@defenddate{#1}}
\newcommand\institute[1]{\def\SUT@value@institute{#1}}
\newcommand\chairman[1]{\def\SUT@value@chairman{#1}}

% 页眉页脚
\pagestyle{fancy}
\fancyhf{}
%\fancyhead[CO]{\songti \zihao{5} \SUT@label@headschoolname}  % 奇数页左页眉
\fancyfoot[C]{\songti \zihao{5} {\thepage}}      % 页脚

\fancypagestyle{plain}{% 设置开章页页眉页脚风格(只有页码作为页脚)
  \fancyhf{}%
  \renewcommand{\headrulewidth}{0pt}
  %\fancyfoot[C]{\songti \zihao{5} \SUT@label@headschoolname}
  \fancyfoot[C]{\songti \zihao{5} {\thepage}} %%首页页脚格式
}


 % 中文摘要
 \newenvironment{abstract}
{
    \cleardoublepage
    \chapter{\SUT@label@abstract}
}{}
%下一页从偶数页开始
 \newcommand\beginatevenpage{
 \clearpage
  \if@twoside
    \thispagestyle{empty}
    \cleardoublepage
  \fi
 }
 % 中文关键词
 \newcommand\keywords[1]{%
   \vspace{2ex}\noindent{\bf \SUT@label@keywords} #1}

 % 英文摘要
 \newenvironment{englishabstract}
{
  \cleardoublepage
  \addtocounter{page}{-1}
  \newgeometry{left=2.2cm,right=3cm}
  \chapter{\rmfamily\SUT@label@englishabstract}
}{
  \restoregeometry
}

 % 英文摘要
 \newcommand\englishkeywords[1]{%
  \vspace{2ex}\noindent{\bf \SUT@label@englishkeywords} #1}


%_ 目录
\renewcommand\tableofcontents{%
    
  \thispagestyle{empty}%
  \hbox{}\newpage
  \addtocounter{page}{-1}

  \if@twocolumn
  \@restonecoltrue\onecolumn
  \else
  \@restonecolfalse
  \fi
  \chapter*{\contentsname}%目录里显示“目录”，否则\chapter*
  \@mkboth{\MakeUppercase\contentsname}{\MakeUppercase\contentsname}%
  \pdfbookmark[0]{目录}{SUTtoc}
  \@starttoc{toc}%
  \if@restonecol\twocolumn\fi
}


%_ 参考文献环境
\renewenvironment{thebibliography}[1]
     {\zihao{5}
      \chapter*{\bibname}
      \@mkboth{\MakeUppercase\bibname}{\MakeUppercase\bibname}%
      \addcontentsline{toc}{chapter}{参考文献}
      \list{\@biblabel{\@arabic\c@enumiv}}%
           {\settowidth\labelwidth{\@biblabel{#1}}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \setlength{\parsep}{1mm}
            \setlength{\labelsep}{0.5em}
            \setlength{\itemsep}{0.05pc}
            \setlength{\listparindent}{0in}
            \setlength{\itemindent}{0in}
            \setlength{\rightmargin}{0in}
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `thebibliography' environment}}%
      \endlist}


\newenvironment{publications}[1]
     {
      \@mkboth{\MakeUppercase\SUT@label@publications}
              {\MakeUppercase\SUT@label@publications}%
      \list{\@biblabel{\@arabic\c@enumiv}}%
           {\settowidth\labelwidth{\@biblabel{#1}}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \setlength{\topsep}{0mm}
            \setlength{\parsep}{0mm}
            \setlength{\labelsep}{0.3em}
            \setlength{\itemsep}{0.05pc}
            \setlength{\listparindent}{0in}
            \setlength{\itemindent}{0in}
            \setlength{\rightmargin}{0in}
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `publications' environment}}%
      \endlist}


\newenvironment{projects}[1]
     {\chapter{\SUT@label@projects}%
      \@mkboth{\MakeUppercase\SUT@label@projects}
              {\MakeUppercase\SUT@label@projects}%
      \list{\@biblabel{\@arabic\c@enumiv}}%
           {\settowidth\labelwidth{\@biblabel{#1}}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `projects' environment}}%
      \endlist}

    \newenvironment{resume}
  {\chapter{\SUT@label@resume}}
  {}

\newenvironment{resumesection}[1]
  {{\noindent\normalfont\bfseries #1}
   \list{}{\labelwidth\z@
           \leftmargin 2\ccwd}
   \item\relax}
   {\endlist}

\newenvironment{resumeli}[1]
  {{\noindent\normalfont\bfseries #1}
   \list{}{\labelwidth\z@
           \leftmargin 4\ccwd
           \itemindent -2\ccwd
           \listparindent\itemindent}
   \item\relax}
   {\endlist}

\newenvironment{conclusion}
  {\chapter*{结论}
    \@mkboth{结论}{结论}%
    \addcontentsline{toc}{chapter}{结论}}
  {}

\renewenvironment{thanks}
  {\chapter{\SUT@label@thanks}
  %\fangsong
  \songti
  }
  {}

\newenvironment{symbolnote}
  {\chapter{\SUT@label@symbolnote}
  \fangsong}
  {}

  %%%============================术语=====================
  \newcommand{\SUT@denotation@name}{主要符号对照表}
  \newenvironment{denotation}[1][2.5cm]{
      \chapter{\SUT@denotation@name} % no tocline
      \noindent\begin{list}{}%
      {\vskip-30bp\zihao{-4}
       \renewcommand\makelabel[1]{##1\hfil}
       \setlength{\labelwidth}{#1} % 标签盒子宽度
       \setlength{\labelsep}{0.5cm} % 标签与列表文本距离
       \setlength{\itemindent}{0cm} % 标签缩进量
       \setlength{\leftmargin}{\labelwidth+\labelsep} % 左边界
       \setlength{\rightmargin}{0cm}
       \setlength{\parsep}{0cm} % 段落间距
       \setlength{\itemsep}{0cm} % 标签间距
      \setlength{\listparindent}{0cm} % 段落缩进量
      \setlength{\topsep}{0pt} % 标签与上文的间距
     }}{\end{list}}
%====增加化学、国际单位宏包
     \RequirePackage[version=4]{mhchem}
     \RequirePackage{siunitx}
\setcounter{secnumdepth}{4}  % 章节编号深度 (part 对应 -1)
\setcounter{tocdepth}{2}     % 目录深度 (part 对应 -1)

\endinput
%%
%% End of file `SUTmaster-xetex.cls'.
